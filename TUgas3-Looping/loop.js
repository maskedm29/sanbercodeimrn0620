//SOAL1 LOOPING WHILE
console.log('===========Soal while===========')
var naik = 0
var turun = 22

console.log('LOOPING PERTAMA')
while(naik < 20){
  naik += 2
  console.log(naik + ' - I love coding')
}

console.log('LOOPING KEDUA')
while ( turun > 2){
  turun -= 2
  console.log(turun + ' - I love coding')
}

//soal2 Looping FOr
console.log('===========Soal For===========')

for(var angka = 1; angka <= 20; angka++) {
  if(angka%3 == 0 && angka%2==1){
    console.log(angka + ' - I love coding')
  }
  else if((angka%2) === 0){
    console.log(angka + ' - Berkualitas')
  }
  else if(angka%3 || angka%2){
    console.log(angka + ' - santai')
  }
} 

//soal3
console.log('===========Soal PErsegi===========')
var persegi =''
for (var x=0; x<4; x++){
  for (var y=0; y<8; y++){
  persegi +='#'
  }
  persegi +='\n'
}
console.log(persegi)

//soal4
console.log('===========Soal Tangga===========')
var tangga ='#'
for(var angka4 = 1; angka4 <=7; angka4++){
  console.log(tangga)
  tangga = tangga + '#'
}


//soal5

console.log('===========Soal Catur===========')
var catur =''
for (var x=0; x < 8; x++) {
  for (var y=0; y<8; y++){
  if((y%2 == 0 && x%2 == 0) || (x%2 == 1 && y%2 == 1)){
    catur += ' '
  }
  else {
    catur += '#'
  }
  }
  catur += '\n'

}

console.log(catur)  