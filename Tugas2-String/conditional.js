//soal if else
var nama = "John"
var peran = ""

// Output untuk Input nama = '' dan peran = ''
"Nama harus diisi!"
 
//Output untuk Input nama = 'John' dan peran = ''
"Halo John, Pilih peranmu untuk memulai game!"
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
"Selamat datang di Dunia Werewolf, Jane"
"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
"Selamat datang di Dunia Werewolf, Jenita"
"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
"Selamat datang di Dunia Werewolf, Junaedi"
"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" 

function soalIfElse(nama, peran){
if (nama == '') {
    console.log('Nama harus diisi!')
}
else if (nama && peran == '') {
    console.log('Halo' + nama + ', pilih Peranmu untuk memulai game!')
}
else if (nama == 'jane' && peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}
else if (nama == 'Jenita' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
else if (nama == 'Junaedi' && peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}
}

console.log('Soal If Else1 ==========')
soalIfElse('', '')

console.log('Soal If Else2 ==========')
soalIfElse('John', '')

console.log('Soal If Else3 ==========')
soalIfElse('Jane', 'Penyihir')

console.log('Soal If Else4 ==========')
soalIfElse('Jenita', 'Guard')

console.log('Soal If Else5 ==========')
soalIfElse('Junaedi', 'Werewolf')

// Soal Switch case
console.log('Soal Switch case ==========')
var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 5; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var teksbulan;

switch (true) {
    case (tanggal < 1 || tanggal > 31): {
        console.log('Input Tanggal Salah')
        break;
    }
    case (tahun < 1900 || tahun > 2200): {
        console.log('Input Tahun Salah')
        break;
    }
    case (bulan > 12 || bulan < 1):
        console.log('Input Bulan Salah')
        break;
    default:
    {
        switch (true) {
            case bulan == 1:
                teksbulan = 'Januari';
                break;
            case bulan == 2:
                teksbulan = 'Februari';
                break;
            case bulan == 3:
                teksbulan = 'Maret';
                break;
            case bulan == 4:
                teksbulan = 'April';
                break;
            case bulan == 5:
                teksbulan = 'Mei';
                break;
            case bulan == 6:
                teksbulan = 'Juni';
                break;
            case bulan == 7:
                teksbulan = 'Juli';
                break;
            case bulan == 8:
                teksbulan = 'Agustus';
                break;
            case bulan == 9:
                teksbulan = 'September';
                break;
            case bulan == 10:
                teksbulan = 'Oktober';
                break;
            case bulan == 11:
                teksbulan = 'November';
                break;
            case bulan == 12:
                teksbulan = 'Desember';
                break;
        }
        console.log('senin', tanggal, ' ', teksbulan, ' ', tahun)
    }
}