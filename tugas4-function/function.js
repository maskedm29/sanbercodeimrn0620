//Soal1
console.log("===================================")
function teriak( kata='Halo sanbers') { 
    return kata
  }
   
  console.log(teriak()) 
  console.log("===================================")

//soal2
function kalikan(num1, num2){
    return num1 * num2
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 30 
console.log("===================================")

//soal3
function introduce(name, age, address, hobby) {
    return 'nama saya' + name +',' + ' umur saya ' + age+',' + ' alamat saya di ' + address +','+ ' dan saya punya hobi yaitu' + hobby+'!'
}
var name = " Agus"
var age = 30
var address = " Jln. Malioboro, Yogyakarta"
var hobby = " Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
console.log("===========================================================================================================================================")
